# README #

## Installation ##

Pour installer et démarrer l'environnement, rien de plus simple :

Placez-vous dans le répertoire du projet et effectuez la commande suivante :


```
#!sh

pip install -r requirements.txt
```

Cela va installer toutes les dépendances nécessaires.

Ensuite une fois votre base de données configurer dans le fichier formation/settings.py, il vous faudra synchroniser vos bases de la manière suivante :


```
#!sh

./manage.py syncdb
./manage.py migrate blog
```

Et voilà, vous n'avez plus qu'à lancer le tout :


```
#!sh

./manage.py runserver
```
