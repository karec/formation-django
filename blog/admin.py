from django.contrib import admin
from blog.models import Article
from blog.forms import ArticleForm


class ArticleAdmin(admin.ModelAdmin):

    list_display = ['title', 'author', 'created', 'updated']
    search_fields = ['title', 'author__username']
    form = ArticleForm

    def save_model(self, request, obj, form, change):
        obj.author = request.user
        obj.save()

admin.site.register(Article, ArticleAdmin)