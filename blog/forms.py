# -*- coding:utf-8 -*-
__author__ = 'evalette'

from django import forms
from blog.models import Article


class ArticleForm(forms.ModelForm):

    class Meta:
        model = Article
        exclude = ['author']
        help_texts = {
            'title' : 'Ce champs à un nombre max de 150'
        }
        error_messages = {
            'title' : {
                'max_length' : 'on avait pas plus de 150',
                'required': 'on en a besoin'
            }
        }