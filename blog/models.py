# -*- coding:utf-8 -*-
from django.db import models
from django.contrib.auth.models import User


class Article(models.Model):
    """
    My article model
    """
    title = models.CharField(max_length=100, db_index=True, verbose_name="Titre")
    content = models.TextField(verbose_name="Contenu")
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    author = models.ForeignKey(User, verbose_name="Auteur")
    img = models.ImageField(upload_to='images')

    def __unicode__(self):
        return self.title
