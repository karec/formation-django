__author__ = 'evalette'
from blog.views import ViewArticle, Index
from django.conf.urls import patterns, url

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'formation.views.home', name='home'),
    url(r'^article/(?P<pk>[\d+])', ViewArticle.as_view(), name='detail'),
    url(r'^index/', Index.as_view(), name='index')
)