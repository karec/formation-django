from django.views.generic import ListView, DetailView
from blog.models import Article


class Index(ListView):

    template_name = 'index.html'
    model = Article
    paginate_by = '5'


class ViewArticle(DetailView):
    model = Article
    template_name = 'detail.html'